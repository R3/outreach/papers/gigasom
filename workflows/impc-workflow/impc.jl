
using Distributed
using GigaSOM
using ClusterManagers
import Glob
import Dates
import NearestNeighbors
import Distributions

using Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

files = Glob.glob("TC_SPL_*.fcs")
#rasterSize=(512, 512) #testing
#alpha=0.1
rasterSize=(2048, 2048)
alpha=0.003

nWorkers = parse(Int, ENV["SLURM_NTASKS"])
addprocs_slurm(nWorkers, topology=:master_worker)

@everywhere using GigaSOM

@info "loading started: $(Dates.now())"
dataset = loadFCSSet(:IMPCdata, files)
@info "loading finished: $(Dates.now())"

@info "transformation started: $(Dates.now())"
dselect(dataset, Vector{Int}(1:18))
dtransform_asinh(dataset, Vector{Int}(7:18), 500.0)
dscale(dataset, Vector{Int}(1:18))
@info "transformation finished: $(Dates.now())"

@info "SOM training started: $(Dates.now())"
som = initGigaSOM(dataset, 32, 32)
som = trainGigaSOM(som, dataset, epochs=30, rFinal=0.1, radiusFun=expRadius(-20.0), knnTreeFun=NearestNeighbors.BallTree)
@info "SOM training finished: $(Dates.now())"

@info "embedding started: $(Dates.now())"
e = embedGigaSOM(som, dataset, k=16, output=:embeddedData)
@info "embedding finished: $(Dates.now())"

@info "embedding export started: $(Dates.now())"
distributed_export(e)
@info "embedding export finished: $(Dates.now())"

using GigaScatter
@everywhere using GigaScatter

dist=Distributions.Normal(0.0, 1.0)

@info "rasterization started: $(Dates.now())"
compound_raster = mixedRaster(distributed_mapreduce(
    [dataset, e],
    (d, e) -> begin
        colors = hcat(  
            Distributions.cdf.(dist, d[:,10]),
            Distributions.cdf.(dist, d[:,16]),
            Distributions.cdf.(dist, d[:,15]),
            fill(alpha, size(d,1)))
        mixableRaster(rasterize(
            rasterSize,Matrix{Float64}(e'),
            Matrix{Float64}(colors'),
            xlim=(-2,33),
            ylim=(-2,33)))
    end,
    mixRasters))
@info "rasterization finished: $(Dates.now())"

savePNG("Result.png", compound_raster)
@info "PNG saved at: $(Dates.now())"
