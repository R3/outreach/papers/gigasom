
using Distributed
using GigaSOM
using ClusterManagers
import CSV
import Tables
import Glob

files = Glob.glob("XXXmyfile*.fcs", "XXXthe/path/to/the/directory/with/files") #TODO fill in

@info "Found files:" files

# TODO for testing purposes you may want to reduce this a bit
# files = files[1:2]

using Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

slurm_ntasks = parse(Int, ENV["SLURM_NTASKS"])
addprocs_slurm(slurm_ntasks, topology=:master_worker)
@everywhere using GigaSOM

#this expects that the data columns are exactly the same in all files, it will produce invalid data otherwise.
di = loadData(:myFiles, files)

# select the relevant columns (fill in the numbers by hand)
dselect(di, Vector(5:30)) #TODO

# transform the rest of the columns (TODO: rewrite 20 to actual number of columns)
dtransform_asinh(di, Vector(1:20), 5)

# scale the dataset columns
dscale(di, Vector(1:20))

som = initGigaSOM(di, 15, 15)

@info "Training!"
som = trainGigaSOM(som, di)

CSV.write("somcodes.csv", Tables.table(som.codes))

@info "Mapping!"
result = distributed_collect(mapToGigaSOM(som, di))

@info "Getting the mapping results!"
open("cell-cluster", "w") do f
    for r in result
        write(f, "$r\n")
    end
end

@info "Creating a cell-to-file mapping"
sizes = loadFCSSizes(files)
open("cell-file", "w") do f
    for (fileIdx,size) in enumerate(sizes)
        for i in 1:size
            write(f, "$fileIdx\n")
        end
    end
end
