REFS=bib/paper-refs.bib
INCLUDES=$(wildcard src/*.tex)
DATADIR=data
DATA=$(wildcard data/*)
PLOTSDIR=plots
RSCRIPTS=$(wildcard $(PLOTSDIR)/*.R)
PLOTSCRIPTS=$(wildcard $(PLOTSDIR)/*.png.R) $(wildcard $(PLOTSDIR)/*.pdf.R)
PLOTS=$(PLOTSCRIPTS:.R=)
MAIN=main.tex
SUPP=supplementary.tex
RXIV=biorxiv.tex
OUTMAIN=$(MAIN:.tex=.pdf)
OUTSUPP=$(SUPP:.tex=.pdf)
OUTRXIV=$(RXIV:.tex=.pdf)
OUT=$(OUTMAIN) $(OUTSUPP) $(OUTRXIV)

DOCKERURL=git-r3lab.uni.lu:4567
LATEXDOCKER=$(DOCKERURL)/r3/docker/latex
RGGDOCKER=$(DOCKERURL)/r3/docker/r-ggplot2
ZOTERODOCKER=$(DOCKERURL)/r3/apps/zotero

USER=$(shell id -u)
GROUP=$(shell id -g)
DRUNUSER=docker run -u $(USER):$(GROUP)

.PRECIOUS: $(PLOTS) $(REFS)

.PHONY: all

.SUFFIXES:

all: $(OUT)

$(OUTMAIN): $(MAIN) $(INCLUDES) $(REFS) $(PLOTS)
	$(DRUNUSER) -it -v $(PWD):/paper -w /paper $(LATEXDOCKER) /bin/sh -c "pdflatex $< && bibtex $(<:.tex=) && pdflatex $< && pdflatex $<"

$(OUTRXIV): $(RXIV) $(INCLUDES) $(REFS) $(PLOTS)
	$(DRUNUSER) -it -v $(PWD):/paper -w /paper $(LATEXDOCKER) /bin/sh -c "pdflatex $< && bibtex $(<:.tex=) && pdflatex $< && pdflatex $<"

$(OUTSUPP): $(SUPP) $(PLOTS)
	$(DRUNUSER) -it -v $(PWD):/paper -w /paper $(LATEXDOCKER) /bin/sh -c "pdflatex $< && pdflatex $<"

$(PLOTSDIR)/%.pdf: $(RSCRIPTS) $(DATA)
	$(DRUNUSER) -t -v $(PWD)/$(DATADIR):/$(DATADIR) -v $(PWD)/$(PLOTSDIR):/$(PLOTSDIR) $(RGGDOCKER) R --vanilla -f $@.R

$(REFS):
	$(DRUNUSER) -t -v $(PWD)/bib/zotero.ini:/config/zotero.ini -v $(PWD)/bib:/bib $(ZOTERODOCKER)

plots: $(PLOTS)

refs:
	rm -f $(REFS)
	$(MAKE) $(REFS)

view:
	open $(OUT)

clean:
	bash -c 'rm -fv *.{log,dvi,aux,toc,lof,out,blg,bbl} $(OUT) $(REFS)'
	rm -fv $(PLOTS)
