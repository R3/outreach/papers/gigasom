# Copyright (C) 2020 Miroslav Kratochvíl <exa.exa@gmail.com>
# Source code released under CC0 license.

source('bench-common.R')

ggsave('bench.pdf', units='in', scale=2, width=6.5, height=4,
  ggplot(d[d$tree=='brute' & d$action=='SOM training' & d$first==FALSE,],
    aes(x=workers, y=ctc, color=factor(datapw))) +
  stat_summary(fun.y = "median", geom='line') +
  geom_jitter(width=0.1, height=.0) +
  facet_grid(som~dim, scales='free_y') +
  scale_x_log10() + scale_y_log10(labels=function(x)f2si(x, unit='s')) +
  scale_color_manual(values=hcl(h=seq(100,270, length.out=3), c=seq(50,70,length.out=5), l=seq(80,25,length.out=3)), name='Cells per process') +
  xlab("# distributed processes") +
  ylab("Aggregate CPU time spent per cell processed") +
  ggtitle("CPU time consumption of distributed SOM training") +
  cowplot::theme_cowplot() +
  theme(
    panel.grid.major=element_line(size=.2, color='#cccccc'),
    axis.text.x = element_text(angle = 60, hjust=1),
    panel.spacing = unit(0.2, "in"),
    legend.position='top')
)
