#!/bin/bash
# Copyright (C) 2020 Miroslav Kratochvíl <exa.exa@gmail.com>
# Source code released under CC0 license.

minutes_const=30
minutes_dim=5
minutes_worker=2
min_minutes=120
mem_const=1024
min_mem=2048

for kdata in 100 200 300 ; do
for dim in 10 20 30 40 50; do
for tasks in 1 2 4 8 16 32 64 128 256 ; do
	testname=n${tasks}-${dim}x${kdata}
	cells_per_task=$((kdata*1024))
	mem_per_dim=$((3 * cells_per_task * 4 /1024/1024))
	exp_minutes=$(( minutes_const + tasks * minutes_worker + dim * minutes_dim ))
	[ $exp_minutes -le $min_minutes ] && exp_minutes=$min_minutes
	[ $exp_minutes -ge 1440 ] && continue
	exp_mem=$(( mem_const + dim * mem_dim))
	[ $exp_mem -le $min_mem ] && exp_mem=$min_mem
	cat > bg-${testname}.sbatch << EOBATCH
#!/bin/bash -l
#SBATCH -n ${tasks}
#SBATCH -c 1
#SBATCH -t ${exp_minutes}
#SBATCH --mem-per-cpu ${exp_mem}M
#SBATCH -J GSOM-${testname}

module load lang/Julia/1.3.0

export BENCHMARK_DIR=tmpfiles-${testname}
mkdir -p \$BENCHMARK_DIR
export BENCHMARK_LOG=gen-${testname}.log
export BENCHMARK_DIM=${dim}
export BENCHMARK_DATA_PER_WORKER=${cells_per_task}
julia benchmark-gen.jl
EOBATCH
done
done
done
