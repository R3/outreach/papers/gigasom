# Copyright (C) 2020 Miroslav Kratochvíl <exa.exa@gmail.com>
# Source code released under CC0 license.

using Distributed, GigaSOM, GigaScatter, ClusterManagers
import Glob, NearestNeighbors, Distributions

# find the file names to process
files = Glob.glob("TC_SPL_*.fcs")

# setup output constants
rasterSize = (2048, 2048)
alpha = 0.003

# read the number of available workers from environment and start the worker processes
n_workers = parse(Int, ENV["SLURM_NTASKS"])
addprocs_slurm(n_workers, topology=:master_worker)

# load the required packages on all workers
@everywhere using GigaSOM
@everywhere using GigaScatter

# load the data to workers
@time dataset = loadFCSSet(:IMPCdata, files)

# collect the total number of cells from all workers
dataset_size = distributed_mapreduce(dataset, d->size(d,1), +)
@info "Loaded total $dataset_size cells."

# preprocess the distributed data
dselect(dataset, Vector(1:18)) # columns 1-18 contain interesting information
dtransform_asinh(dataset, Vector(7:18), 500.0) # transform the expressions
dscale(dataset, Vector(1:18)) # scale all columns

# train the SOM and run the embedding
som = initGigaSOM(dataset, 32, 32)
@time som = trainGigaSOM(som, dataset, epochs=30,
                   rFinal=0.1, radiusFun=expRadius(-20.0),
                   knnTreeFun=NearestNeighbors.BallTree)
@time e = embedGigaSOM(som, dataset, k=16, output=:embeddedIMPC)

# convert the embedding into rasters in-place
dist=Distributions.Normal()

# run the distributed rasterization
@time compound_raster = mixedRaster(distributed_mapreduce(
    [dataset, e],
    (d, e) -> begin
        # convert the normalized expressions to numbers between 0--1
        colors = hcat(
            Distributions.cdf.(dist, d[:,10]), # R: CD8 column
            Distributions.cdf.(dist, d[:,16]), # G: CD4 column
            Distributions.cdf.(dist, d[:,15]), # B: CD161 column
            fill(alpha, size(d,1)))
        # rasterize the local part of the data
        mixableRaster(rasterize(
            rasterSize,
	    Matrix{Float64}(e'),
            Matrix{Float64}(colors'),
            xlim=(-2,33), ylim=(-2,33)))
    end,
    mixRasters))

# save the result to a PNG
savePNG("impc-embedding.png", compound_raster)
