# Copyright (C) 2020 Miroslav Kratochvíl <exa.exa@gmail.com>
# Source code released under CC0 license.

using GigaSOM
using DelimitedFiles
import Random

function doCluster(fn::String)
    d = readdlm("data-$fn.tab")
    for seed in 1:100
        @info "iteration" seed
        Random.seed!(seed)
        som = initGigaSOM(d, 20, 20)
        som = trainGigaSOM(som, d)
        map = mapToGigaSOM(som, d)
        
        writedlm("gs-clust-$fn-$seed.tab", map[:,1], ' ')
    end
end

doCluster("Levine_13dim.fcs")
doCluster("Levine_32dim.fcs")
