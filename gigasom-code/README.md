
# GigaSOM.jl analysis code

This archive contains GigaSOM.jl source code archived at the time of submitting
the manuscript, together with Julia and R scripts that were used to generate
the results presented in the manuscript.

Contents:
- `GigaSOM.jl/` -- GigaSOM source code
- `GigaScatter.jl/` -- GigaScatter source code (used for plotting the large embedding)
- `clustering/` -- The benchmark of clustering precision (FlowSOM vs. GigaSOM)
- `performance/` -- Performance and scalability benchmark
- `impc/` -- the code used to produce and plot the image of IMPC dataset (Figures 1c and S4)

## Usage

`GigaSOM.jl` and `GigaScatter` directories contain the corresponding Julia
packages that can be installed by the usual Julia means; versions obtained by
the Julia package manager (or from GitHub) may be slightly updated but should
generally produce the same output.

The installation of GigaSOM is detailed in the separate file
`GigaSOM.jl/README.md`, together with simple usage instructions and expected
outputs. The same information is available online at
https://github.com/LCSB-BioCore/GigaSOM.jl .

GigaSOM documentation (as referenced in the `GigaSOM.jl/README.md`) may be
built by executing `julia make.jl` in the directory `GigaSOM.jl/docs/`, the
HTML files will appear in subdirectory `build/`.

### Performance benchmark

The performance benchmark is performed as follows:

1. the script `makebench-gen.sh` is executed to generate many instances of
   Slurm batch files, these describe multiple executions of the benchmark
   script (in `benchmark-gen.jl`) with different parameters
2. all batch files are submitted to Slurm for execution (using e.g.
   `sbatch *.sbatch`)
3. after all computation is done, the partial log files are concatenated to
   form the `benchmark-results` file (the `performance/` directory contains
   this file with our raw measurements)
4. the resulting file is processed by the `bench-mini.pdf.R` and `bench.pdf.R`
   scripts in the directory, to produce the plots presented in the manuscript.
   `bench-common.R` contains the common code that loads and prepares the
   `benchmark-results` file.

### Clustering comparison with FlowSOM

The clustering comparison aims to verify that the SOM implementation in GigaSOM
is able to produce results of the same quality as FlowSOM. We show that on the
Levine13 and Levine32 datasets, by measuring that GigaSOM (with default
parameters) does _not_ produce worse match to the ground truth (the manual
gating) than FlowSOM. Moreover, because the FlowSOM-style metaclustering is
completely separate from the SOM implementation, the clustering quality is only
measured on the SOM-originating clusters (sometimes termed "pre-clusters").

The comparison is performed as follows:

1. Files `Levine_13dim.fcs` and `Levine_32dim.fcs` are downloaded from
   https://flowrepository.org/id/FR-FCM-ZZPH to the `clustering/` directory.
2. Script `1-prepare-data.R` is executed to parse the files into plain tabular
   data (`.tab`) for later benchmarking
3. Scripts `2-cluster-flowsom.R` and `2-cluster-gigasom.jl` are executed by R
   resp. Julia to produce the clustering information (saved in `.tab` files).
   The computations take around 1 hour on a recent laptop.
4. After the benchmarks are produced, script `3-collect-results.R` is executed
   to plot the figures 1b and S1.

#### Discussion of the result

The final script also computes mean F1 scores from all clusterings produced by
FlowSOM and GigaSOM, and prints out the results of t-test that compares the
score groups. In fact, the testing shows slight advantage of GigaSOM with the
default parameters over FlowSOM with default parameters. We do not consider
this improvement to be of much practical value, for the following reasons:

- The testing only aims to show that GigaSOM does not contain a technical
  disadvantage that would cause it to produce low-quality results,
- the results may be easily influenced by dataset-specific parameter tuning in
  both FlowSOM and GigaSOM,
- the actual absolute difference of means between the two distributions of mean
  F1 scores is only around 0.03 and 0.01 on Levine13 resp. Levine32.

These reasons led us to mark the results as "no significant difference" in the
manuscript.

### IMPC dataset processing

`impc.jl` is designed to be executed within the distributed Slurm environment.
Additionally from the manuscript (listing S1), it contains the `@time` commands
that print out the timing of individual computation parts.

The script can be executed also as a stand-alone (or within other distributed
computing environment), by replacing the Slurm-depending commands on lines
12-13 by some other method of adding workers, e.g.
```julia
addprocs(16)
```
...for adding 16 local workers. This will likely fail to process the whole
dataset due to memory limitations, but may succeed with processing small
subsets thereof.

Running the script successfully in the distributed environment on the whole
dataset requires the following:
- The corresponding IMPC T-cell spleen dataset downloaded from
  http://flowrepository.org/id/FR-FCM-ZYX9 with 2905 FCS files, saved in the
  same directory as where the script is executed
- Presence on a running Slurm cluster with enough nodes and memory to contain
  the dataset. We used 256 CPU cores, each with assigned 3GB of memory.

To run the script, copy the file `impc.sbatch` and `impc.jl` into the directory
with the downloaded FCS files, and execute `sbatch impc.sbatch`. After getting
the resource allocation within the cluster, the processing should finish within
several tens of minutes (on our testing cluster, the total processing time was
under 30 minutes, but the time may vary depending on many factors, including
the properties of available hardware). The reproduced picture of the dataset
should appear in file `impc-embedding.png`.
