\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[mono=false]{libertine}
\usepackage[super,sort&compress]{natbib}
\bibliographystyle{unsrtnat}
\citestyle{nature}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage[a4paper,left=3cm,right=3cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{lineno}
\linenumbers

\def\linenumberfont{\normalfont\tiny\sffamily\color{blue!50}}

\newcommand{\xxx}[1]{\textcolor{red}{#1}}
\emergencystretch=2em
\hyphenpenalty=10000

\title{\bfseries Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl}
\author{Miroslav Kratochvíl
\thanks{Correspondence: \texttt{miroslav.kratochvil@uochb.cas.cz}, \texttt{oliver.hunewald@lih.lu}}
\thanks{Institute of Organic Chemistry and Biochemistry, Prague, Czech Republic}
\thanks{Department of Software Faculty of Mathematics and Physics, Charles university, Prague, Czech Republic}
\and Oliver Hunewald \footnotemark[1]
\thanks{Department of Infection and Immunity, Luxembourg Institute of Health, Esch-sur-Alzette, Luxembourg}
\and Laurent Heirendt
\thanks{Luxembourg Centre for Systems Biomedicine, University of Luxembourg, Esch-sur-Alzette, Luxembourg}
\and Vasco Verissimo \footnotemark[5]
\and Jiří Vondrášek \footnotemark[2]
\and Venkata P. Satagopam \footnotemark[5]
\and Reinhard Schneider \footnotemark[5]
\and Christophe Trefois \footnotemark[5]
\and Markus Ollert \footnotemark[4]
\thanks{Department of Dermatology and Allergy Center, Odense Research Center for Anaphylaxis, Odense University Hospital, University of Southern Denmark, Odense, Denmark}
}
\date{}

\begin{document}
\maketitle
\begin{abstract}
Advances in single-cell cytometry technology yield datasets of constantly growing size and dimensionality, requiring appropriate support in computational analysis methods.
We have developed GigaSOM.jl, a Julia package for horizontally-scalable SOM-based analysis that unlocks processing of datasets of previously unreachable sizes.
Utilizing the high-performance computing infrastructure, GigaSOM.jl is the first cytometry software to allow rapid and convenient clustering and visualization of even the largest public datasets with over 1.1 billion events.
\end{abstract}

Flow and mass single-cell cytometry measures abundance of surface and intra-cellular molecular features of single cells in a suspension.
Given the diversity of markers available for detection of various biochemical properties, it has become an indispensable method for many fields of study, including immunology, molecular biology, and marine biology.
The advances in technology and sample processing, e.g., the recent adoption of spectral flow cytometry and other measurement technologies~\cite{robinson_flow_2015,schmutz_spectral_2016,zhang_stimulated_2017}, allow for inexpensive measurement of more than 30 parameters in tens of millions of single cells in a given sample. Consequently, the datasets originating from relatively small experiments and even single-patient therapeutical screenings, such as in the emerging personalized immunology~\cite{delhalle_roadmap_2018}, may easily reach billions of total events.
Precise analysis of the full-volume samples is crucial for the detection of many extremely rare cell types and intermediate cell development states.
Since the manual analysis of highly parametric data is laborious and often imprecise, many automated tools have been developed to reduce the data complexity overhead by means of clustering, dimensionality reduction, difference testing, or pathway inference~\cite{saeys_computational_2016,montante_flow_2019}.

The FlowSOM package~\cite{gassen_flowsom:_2015}, implemented in R, has gained considerable usage since its introduction, mainly for its computational efficiency and high clustering quality derived from the use of self-organizing maps (SOMs)~\cite{liu_comparison_2019,weber_comparison_2016}.
Despite the remarkable efficiency, the present technological limits of FlowSOM prevent it from scaling to datasets with more than several tens of millions of cells. This is due partly because of the inherently single-threaded computation that makes the processing time restrictive, and partly because of the R software environment that fails to efficiently handle giga-scale datasets.

Here, we present GigaSOM.jl, a cytometry-oriented implementation of the SOMs that serves as a significantly faster and more scalable alternative to FlowSOM.
The main aim of GigaSOM.jl is to allow convenient analysis of datasets that contain billions of individual cells. To achieve this goal, GigaSOM.jl implementation avoids various bottlenecks that limit the computation speed and dataset capacity, and provides a powerful data analysis toolkit that can utilize the computational resources commonly available to scientists, namely multi-core systems and high-performance computing (HPC) clusters~\cite{varrette_management_2014}.

The scalability is promoted by two main design considerations:
First, all computational tasks in the workflow (dataset preparation, normalization, clustering, visualization and statistics collection) are modified to run in parallel and utilize any number of available CPUs.
Second, the loaded datasets are divided into smaller parts and assigned to separate computational resources, thus minimizing per-CPU memory load, and allowing to process larger datasets without any specialized high-memory hardware.
Most importantly, GigaSOM.jl implementation is distributed and scales horizontally.
In effect, resource requirements for processing a dataset grow linearly with dataset size, and sufficient resources to process even very large datasets can be built by interconnecting inexpensive, office-grade hardware into ad-hoc computer clusters.

We have evaluated the applicability of GigaSOM.jl by verifying the quality of the output clustering and by benchmarking the capability to scale horizontally. The tests thus ensure the relevance of obtained results, and confirm the potential for unlocking the analyses of huge datasets that were so far not computationally feasible.

\begin{figure*}
\begin{tabular}{p{.02\linewidth}p{.95\linewidth}}
\vspace{0pt}\textbf{a}&\vspace{0pt}\includegraphics[width=\linewidth]{plots/clustering-mini.pdf}
\end{tabular}
\begin{tabular}{p{.02\linewidth}p{.45\linewidth}p{.02\linewidth}p{.45\linewidth}}
\vspace{0pt}\textbf{b}&\vspace{0pt}\includegraphics[width=\linewidth]{plots/bench-mini.pdf}&
\vspace{0pt}\textbf{c}&\vspace{0pt}\includegraphics[width=\linewidth]{media/embedding.jpg}\\
\end{tabular}
\caption{Properties of GigaSOM.jl output.
\textbf{a}~Typical result of FlowSOM and GigaSOM.jl clustering compared to the manual analysis of Levine32 dataset.
Confusion matrix is normalized in columns, showing the ratio of each aggregate SOM-originating cluster matching the manually gated cell population.
Most variance in the mean F1 scores can be attributed to random initialization of the algorithms; the scores achieved by both algorithms do not differ significantly.
\textbf{b}~GigaSOM.jl performance scales well with number of available processing units.
The measurements of speed (points) are plotted as cells processed per second, lines interpolate mean performance per group.
\textbf{c}~EmbedSOM rendering of all 1.1 billion cells contained in the IMPC Spleen T cell dataset computed using GigaSOM.jl.
Expression of three main markers is displayed in combination as mixed colors; CD8 in red, CD4 in green, and CD161 in blue.}
\label{fig:res}
\end{figure*}

The most important algorithmic change between FlowSOM and GigaSOM.jl is the switch from the stepwise-recursive SOM training algorithm to the highly parallelizable batch training~\cite{kohonen_essentials_2013}.
To quantify the possibly undesirable differences in the outputs of both training methods, we used a benchmark similar to that of~\citet{weber_comparison_2016}, who assessed the clustering quality by computing the maximal possible mean F1 score between manual classification and the unsupervised clustering.
In a typical benchmark outcome, most populations were matched by GigaSOM.jl just as well as by FlowSOM, as displayed on Levine32 dataset in~\cref{fig:res}a.
(Typical results from both datasets are available in supplementary fig.~S1.)
Both methods consistently achieved mean F1 scores in the ranges 0.65--0.7 on Levine13 dataset and 0.81--0.84 on Levine32 dataset for a wide range of tested parameter settings.
In the tests, neither algorithm showed significantly better resulting mean F1 scores.

To evaluate the resource usage and scalability potential, we have performed several benchmarks on large randomly generated datasets in a distributed environment.
These covered a wide range of used CPUs (1--256), data density (100--300 thousand cells per node), dataset dimensionality (10--50 dimensions) and SOM sizes (10\texttimes10 to 40\texttimes40).
In each benchmark, the dataset was distributed equally among the chosen number of CPUs, processed by the SOM training algorithm, and the resulting SOM was used for classifying the dataset into clusters and producing an embedded view using EmbedSOM~\cite{kratochvil_generalized_2019}.
All algorithms were also tested in variants where the internal nearest-neighbor search was accelerated by a spatial-indexing data structure, in particular by the kd-trees and ball-trees~\cite{rezaabbasifard_survey_2014}.

In the performance benchmark, the SOM training speed scaled almost linearly with added computing resources, thus confirming horizontal scalability of GigaSOM.jl.
The detailed performance benchmark additionally showed that the scaling holds also for decreased data density and increased dataset dimension (the measurements are available in Supplementary Figure~S2).

By utilizing the spatial-indexing data structures, we have been able to achieve performance gain in cases where the spatial indexing efficiency was not impeded by dimensionality overhead, and the SOM size was sufficient to make the asymptotic improvement observable (see Supplementary Figure~S3 for details).
In particular, large 40\texttimes40 SOMs were accelerated by use of both ball-trees (speedup around 2.5\texttimes) an kd-trees (1.7\texttimes) on datasets with number of parameters common in current flow cytometry experiments (cca.~10--20).
The performance advantage of spatial indexing disappeared for SOMs smaller than 20\texttimes20 and for more than 30 dimensions.

Finally, to showcase the new data processing possibilities, we have used GigaSOM.jl to train a SOM on a dataset from the International Mouse Phenotyping Consortium (IMPC), and visualize it using the EmbedSOM method.
The dataset consists of 2905 samples collected in a systematic CRISPR-Cas9-based single-gene knockout~\cite{ran_genome_2013} phenotyping study from spleens of the genetically modified mice, stained by a mix of T cell targeting fluorescent markers~\cite{munoz-fuentes_international_2018}, totally counting 1,167,129,317 cells with 11 fluorescent markers.
Notably, the whole operation, consisting of program initialization, data loading (82.6GB of FCS files), SOM training for 30 epochs, embedding and export of embedded data (17.4GB) took slightly less than 25 minutes on a relatively small 256-CPU-core computer cluster (distributed over 11 physical server nodes), and consumed at most 3GB of RAM per core.
From that, each epoch of the distributed SOM training took approximately 25 seconds, and computation of EmbedSOM visualization took 3 minutes.
Data rasterization, aggregation and plotting of the result was performed using GigaScatter.jl package used within the GigaSOM.jl framework; distributed rasterization and combination of partial rasters took slightly over 4 minutes. The resulting plot with 1.1 billion cells can be observed in~\cref{fig:res}c.

The output shows clearly identifiable populations of CD4\textsuperscript{+} and CD8\textsuperscript{+} T cells, NK and NKT cells, and a large body of other cells with no specific staining in the panel, consisting mostly of various other leukocytes, apoptotic cells, doublets and debris.
Using a different set of highlighted markers, we were also able to identify various subsets of CD4\textsuperscript{+} and CD8\textsuperscript{+} T cells (including the effector and resting subtypes of CD4\textsuperscript{+} Thelpers and Tregs, resp. naive and memory CD8s), and of $\gamma\delta$TCR\textsuperscript{+} T cells.
Annotated version of the figure is available in Supplementary Figure~S4.
The SOM-based clustering produced in the analysis may be directly used to obtain precise per-population sample statistics, similar to the one obtained manually in the IMPC study~\cite{cacheiro_new_2019}.

We note that the more traditional approaches to dataset dimensionality reduction, such as tSNE or UMAP, are not directly applicable to data of this size due to their highly unfavorable asymptotic complexity~\cite{becht_dimensionality_2019} (time required for naive computation can be estimated in months).
GigaSOM.jl supports running the approximative combination of the more advanced algorithms with EmbedSOM~\cite{kratochvil_generalized_2019}, which improves the time complexity while still maintaining many relevant properties of dimensionality reduction based on neighborhood optimization.

The results conclusively show that GigaSOM.jl will support the growing demand for high-volume data processing, utilizing the commodity hardware resources widely available in labs and universities.
The ability to process a billion-scale dataset to a comprehensible embedding and precise, easily scrutinizable per-cluster statistics in mere minutes may play a crucial role in both design and analysis methods of cytometry experiments.
We believe that the accessible and flexible nature of the GigaSOM.jl implementation and programming environment will also drive transformation of other tools in the ecosystem towards the support of big-data processing paradigms.

\bibliography{bib/paper-refs}

\section*{Methods} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Horizontally scalable data handling}
GigaSOM.jl implements distribution of large datasets to multiple compute nodes.
The main design goal that ensures horizontal scalability is to prevent memory exhaustion of any single compute node.
Most of the dataset operations are adapted to this behavior and reimplemented to avoid accessing the whole dataset at once.

High-performance loading of large datasets relies on availability of shared storage; that is a common expectation in current HPC environments, moreover the data transfers withing shared storage are often highly optimized for efficiency and throughput.
The loading process first reads header information from all FCS files to obtain precise information about data size; then instructs the compute nodes to access and load only precisely specified portions of the dataset to construct the local data slice.

A MapReduce-style framework is employed for manipulating the distributed data, offering a simple way to run custom distributed data-processing tasks.
Many algorithms useful for cytometry are implemented in GigaSOM.jl tools library, including e.g.~distributed retrieval of per-cluster statistics, efficient distributed median computation algorithm, data transformations and scaling routines, and distributed dataset import and export.

\paragraph{Distributed SOM computation}
GigaSOM.jl uses the standard Batch-SOM training conducted in epochs.
The training maintains a codebook $W$ (represented as a a matrix of row vectors), in each epoch a new version of the codebook is computed according to
\[
W_{i+1} = \hat{H}_i\cdot \mathcal{N}(X,W_i)\cdot X
\]
where $X$ is the dataset row-vector matrix, $\mathcal{N}(X,W_i)$ is a column-scaled neighborhood matrix that marks indexes of codebook vectors closest to each dataset item, and $\hat{H}_i$ is a topology matrix that describes relative neighborhood influence in the codebook in training epoch $i$.

In a parallel environment, the algorithm gains speedup by processing row slices of $X$ on multiple CPUs and postponing multiplication by $\hat{H}_i$.
Spatial indexing data structures (ball trees, KD-trees) are further used to accelerate the computation of $\mathcal{N}(X,W_i)$.
Inter-process synchronization and communication in the algorithm is reduced to broadcasting the $W$ at the start of each epoch, gathering the partial codebook updates for each slice of $X$, and aggregating and multiplying them by $\hat{H}_i$ centrally.

\paragraph{Benchmark methodology and setup}
Validation of clustering relevance was performed on Levine13 and Levine32 datasets, both available from FlowRepository \href{http://flowrepository.org/id/FR-FCM-ZZPH}{\tt FR-FCM-ZZPH}.
The datasets were processed by FlowSOM and GigaSOM.jl SOM implementations to generate 20\texttimes20 SOMs and classify the dataset into corresponding 400 clusters.
In a manner similar to other analyses, we aimed to verify how well the cluster structure fits the manual annotation of the given dataset.
The clusters were aggregated by a greedy algorithm to the expected population count in each dataset (24 aggregated clusters in Levine13, 14 in Levine32), the assignment method preferentially assigned the cluster to a population with its greatest portion contained in the cluster.
Per-population mean F1 score was used to compare each aggregated clustering to the manual annotation.

Performance benchmarks were collected on a Slurm-managed HPC cluster equipped with Intel\textregistered Xeon\textregistered E5-2650 CPUs; each node with 2 physical CPUs (total 24 cores) and 128GB of RAM.
All performance benchmarks were executed several times: first runs were discarded to prevent the influence of caches and Julia just-in-time compilation; the remaining run-times were normalized to medians and used as results.

\paragraph{IMPC Spleen T cell dataset processing}
The full dataset downloaded from FlowRepository \href{http://flowrepository.org/id/FR-FCM-ZYX9}{\tt FR-FCM-ZYX9} (2905 FCS files) was processed using GigaSOM.jl as follows:
cell expressions were compensated as described in individual FCS files, fluorescent marker expressions were transformed by the hyperbolic arcsine transformation with cofactor 500, and all dataset columns were scaled to $\mu=0$ and $\sigma=1$.
This data was used to train a 32\texttimes32 SOM, which was in turn used to produce the embedding using EmbedSOM algorithm with parameter $k=16$.
The computation was run on a Slurm cluster separated into 256 individual single-CPU tasks with 3GB RAM allocated per task.
Data rasterization and plotting was done using GigaScatter.jl package.
Time measurements were taken as wall-clock time.
Detailed workflow is shown in supplementary Listing~S1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Data availability}
The used datasets are available on FlowRepository, under accession IDs
\href{http://flowrepository.org/id/FR-FCM-ZZPH}{\tt FR-FCM-ZZPH} and
\href{http://flowrepository.org/id/FR-FCM-ZYX9}{\tt FR-FCM-ZYX9}.

\subsection*{Code availability}
GigaSOM.jl and GigaScatter.jl source code is available under Apache License 2.0 from
\href{https://doi.org/10.17881/lcsb.z5vy-fa75}{doi:10.17881/lcsb.z5vy-fa75}.

\subsection*{Acknowledgements}
MK and JV were supported by ELIXIR CZ LM2018131 (MEYS).

This work was supported by the Luxembourg National Research Fund (FNR) through the FNR AFR-RIKEN bilateral program (TregBar 2015/11228353) to MO, and the FNR PRIDE Doctoral Training Unit program (PRIDE/11012546/NEXTIMMUNE) to VV, RS and MO.

The Responsible and Reproducible Research (R\textsuperscript{3}) team of the Luxembourg Centre for Systems Biomedicine is acknowledged for supporting the project and promoting reproducible research.

The experiments presented in this paper were carried out using the HPC facilities of the University of Luxembourg~\cite{varrette_management_2014} (see \url{https://hpc.uni.lu}).

The project was supported by Staff Exchange programme of ELIXIR, the European life-sciences infrastructure.

\subsection*{Author contributions}

Conceptualization: OH, LH, CT.
Formal analysis, investigation, methodology: OH, MK, LH.
Software: OH, MK, LH, VV.
Funding acquisition, supervision: JV, VPS, RS, CT, MO.
Validation: OH, MK.
Visualization: MK.
Writing: OH, MK.
All authors participated in reviewing, editing and finalization of the manuscript.

\subsection*{Competing interests}
The authors declare that they have no competing interests.

\end{document}
