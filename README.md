# GigaSOM.jl: Huge-scale, high-performance flow cytometry clustering in Julia

[![pipeline status](https://gitlab.lcsb.uni.lu/R3-core/outreach/gigasomPaper/badges/master/pipeline.svg)](https://gitlab.lcsb.uni.lu/R3-core/outreach/gigasomPaper/commits/master)

## Prerequisites

- Install `git`: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Install `git-lfs`: https://help.github.com/en/github/managing-large-files/installing-git-large-file-storage
- Install `docker`: https://docs.docker.com/install/

## How to get started

Once all prerequisites have been installed, fork and clone the repository:

```bash
$ git clone ssh://git@gitlab.lcsb.uni.lu:8022/<firstname.lastname>/gigasompaper.git gigasompaper
```
Then, create a new branch from the `develop` branch:
```bash
$ cd gigasompaper
$ git checkout -b myBranch origin/develop
```

## Configure Zotero

In the directory `bib`, copy the `template.ini` file and name it `zotero.ini`:

```bash
$ cp bib/template.ini bib/zotero.ini
```
Then, change the content of the `zotero.ini` file by configuring the `[account]` section:
```
[account]
email = firstname.lastname@domain.com
id = 123456
type = user
key = 1a2b3c4d5e6f7g8h9i0jklmnoprstuvwxyz
```
You can retrieve the data by browsing to https://www.zotero.org/settings/keys. Set the `type` to `group` if you want to access
a group library. You can find the group ID by browsing to https://www.zotero.org/groups. The ID is given in the URL of the group.

You can either set your email address linked to your Zotero account or your username listed under https://www.zotero.org/settings/account.

## Generate the paper

Once you have configured Zotero, go ahead and generate the paper:
```bash
$ cd gigasompaper
$ make
```
Please note that `make` runs Docker in the background. The first time, several docker images will be downloaded, which will take some time.
If you want to view the paper, type within the `gigasompaper` folder:
```bash
$ make view
```
If you want to recompile the paper from scratch:
```bash
$ make clean
```

