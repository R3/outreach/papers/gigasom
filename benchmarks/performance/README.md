# GigaSOM performance benchmark (with SLURM)

The scripts in this directory were used to collect the data for performance
scaling benchmark. You need to run this on a working SLURM frontend.

Usage is as follows:

1. generate the slurm batch scripts by executing `makebench-gen.sh`
2. run the whole scriptage and wait for all tasks to finish:
```sh
cp bg-*.sbatch $SCRATCH
cd $SCRATCH
sbatch bg-*.sbatch
```
3. wait until all tasks finish (watch `squeue`). Collect the results just by
   concatenating the log files as such:
```sh
cat gen-*.log > path/to/this/repo/data/benchmark
```
   (file `data/benchmark` in this repo was generated precisely in that way)

The benchmarks do not process too high volumes of data (just what is necessary
to confirm the scaling). If you want to observe and measure the actual 1Gcell
processed, run `sbatch bg-n128-40x8192.sbatch`.
