
using Distributed
using GigaSOM
using ClusterManagers

using Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

testReps = 4 #discard first one and take median of the rest
somSizes = [10, 20, 40]
dataPerTask = parse(Int, ENV["BENCHMARK_DATA_PER_WORKER"])
dim = parse(Int, ENV["BENCHMARK_DIM"])
logFile = ENV["BENCHMARK_LOG"]

log = open(logFile, "w")

cd(ENV["BENCHMARK_DIR"])

nWorkers = parse(Int, ENV["SLURM_NTASKS"])

function doLog(phase, tree, somSize, time)
    write(log, "$phase\t$nWorkers\t$dataPerTask\t$dim\t$tree\t$somSize\t$time\n")
    flush(log)
end

addprocs_slurm(nWorkers, topology=:master_worker)
@everywhere using GigaSOM

#check this out
@info "Distribution:" workers=workers()

distributed_transform(:(()), dummy->randn(dataPerTask, dim), workers(), :benchData)
dInfo = LoadedDataInfo(:benchData, workers())

using NearestNeighbors

for somSize in somSizes
    origSom = initGigaSOM(dInfo, somSize, somSize)

    #SOMs
    for i in 1:testReps
        time = @elapsed som = trainGigaSOM(origSom, dInfo)
        doLog("train", "brute", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed som = trainGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.BallTree)
        doLog("train", "ball", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed som = trainGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.KDTree)
        doLog("train", "kd", somSize, time)
    end

    #mapping
    for i in 1:testReps
        time = @elapsed mapToGigaSOM(origSom, dInfo)
        doLog("map", "brute", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed mapToGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.BallTree)
        doLog("map", "ball", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed mapToGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.KDTree)
        doLog("map", "kd", somSize, time)
    end

    #embedding
    for i in 1:testReps
        time = @elapsed embedGigaSOM(origSom, dInfo)
        doLog("embed", "brute", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed embedGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.BallTree)
        doLog("embed", "ball", somSize, time)
    end

    for i in 1:testReps
        time = @elapsed embedGigaSOM(origSom, dInfo, knnTreeFun=NearestNeighbors.KDTree)
        doLog("embed", "kd", somSize, time)
    end
end

close(log)
