
using GigaSOM
using DelimitedFiles
import Random

function doCluster(fn::String)
    d = readdlm("../data/data-"*fn*".tab")
    Random.seed!(1)
    som = initGigaSOM(d, rStart=20.0, 20, 20)
    #some other attempts:
    #som = trainGigaSOM(som, d, rStart=16.0, rFinal=3.0, epochs=10)
    #som = trainGigaSOM(som, d, rStart=2.5, rFinal=0.2, epochs=10)
    #som = trainGigaSOM(som, d, radiusFun=expRadius(-10.0), kernelFun=bubbleKernel, epochs=20)
    #som = trainGigaSOM(som, d, radiusFun=expRadius(-10.0), kernelFun=bubbleKernel, epochs=20) 
    map = mapToGigaSOM(som, d)
    
    writedlm("../data/gs-clust-"*fn*".tab", map[:,1], ' ')
    writedlm("../data/gs-som-"*fn*".tab", Matrix{Float64}(som.codes), ' ')
end

doCluster("Levine_13dim.fcs")
doCluster("Levine_32dim.fcs")
