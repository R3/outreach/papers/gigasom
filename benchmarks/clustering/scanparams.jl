
using GigaSOM
using Distributed
using DelimitedFiles
import Random

#addprocs(parse(Int64, ENV["SLURM_CPUS_PER_TASK"]))
addprocs(4)
@everywhere using GigaSOM

function doCluster(fn::String; rStart::Float64, radiusFun=linearRadius, kernelFun=bubbleKernel, epochs::Int64=20, tag::String)
    @info "Starting" fn tag
    d = readdlm("../data/data-"*fn*".tab")
    @info "loaded"
    Random.seed!(1)
    som = initGigaSOM(d, 20, 20)
    @info "training"
    som = trainGigaSOM(som, d; rStart=rStart, radiusFun=radiusFun, kernelFun=kernelFun, epochs=epochs)
    @info "mapping"
    map = mapToGigaSOM(som, d)
    @info "writing"
    
    writedlm("../data/clust-"*tag*"-"*fn*".tab", map[:,1], ' ')
    writedlm("../data/som-"*tag*"-"*fn*".tab", Matrix{Float64}(som.codes), ' ')
end

starts=Dict{String,Float64}()
starts["10"]=10.0
starts["16"]=16.0
starts["20"]=20.0

radii=Dict{String,Any}()
radii["linear"]=linearRadius
radii["exp0"]=expRadius()
radii["exp5"]=expRadius(5.0)
radii["exp-5"]=expRadius(-5.0)
radii["exp-10"]=expRadius(-10.0)

kernels=Dict{String,Any}()
kernels["gauss"]=gaussianKernel
kernels["bubble"]=bubbleKernel
kernels["threshold"]=thresholdKernel

epochs=Dict{String,Int}()
epochs["10"]=10
epochs["15"]=15
epochs["20"]=20
epochs["25"]=25

for fn in ["Levine_13dim.fcs", "Levine_32dim.fcs", "Samusik_all.fcs"]
    for (bt,b) in starts
        for (rt,r) in radii
            for (kt,k) in kernels
                for (et,e) in epochs
                    doCluster(fn, rStart=b, radiusFun=r, kernelFun=k, epochs=e,
                        tag=bt*"-"*rt*"-"*kt*"-"*et)
                end
            end
        end
    end
end
