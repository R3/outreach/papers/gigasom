\documentclass{report}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{merriweather}
%\usepackage[mono=false]{libertinus}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{textcomp}
\usepackage[scale=.8]{geometry}
\usepackage{tikz}

\usepackage{listings}
\usepackage{float}

\newfloat{listing}{tbp}{lst}
\floatname{listing}{Listing}

\usepackage{hyperref}

\renewcommand{\thefigure}{S\arabic{figure}}
\renewcommand{\thelisting}{S\arabic{listing}}

\title{Supplementary material for \\\bfseries GigaSOM.jl: High-perforamnce clustering and visualization of huge-scale cytometry datasets}
\author{
Miroslav Kratochvíl \and
Oliver Hunewald \and
Laurent Heirendt \and
Vasco Verissimo \and
Jiří Vondrášek \and
Venkata P. Satagopam \and
Reinhard Schneider \and
Christophe Trefois \and
Markus Ollert
}
\date{}

\begin{document}
\maketitle
\pagenumbering{gobble}

\begin{figure}[p]
\centering
\includegraphics[width=6in]{plots/clustering.pdf}
\caption{Results of clustering precision comparison with FlowSOM, on Levine13 and Levine32 datasets.}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=6.5in]{plots/bench.pdf}
\caption{Detailed overview of the effect of horizontal scaling on the performance of SOM training in GigaSOM.jl. The color distinguishes different benchmarked levels of CPU occupancy (100 to 300 thousand cells loaded in memory per CPU).}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=6.5in]{plots/trees.pdf}
\caption{Detailed overview of the speedups provided by using spatial indexes for accelerating the nearest neighbor queries in implemented algorithms.}
\end{figure}

\begin{figure}[p]
\centering
\tikzstyle{label}=[draw, fill=white, rectangle, rounded corners=.5ex]
\tikzstyle{arr}=[->, thick]
\begin{tikzpicture}[font=\tiny\sf]
\node[inner sep=0pt] (pic) {\includegraphics[width=\linewidth]{media/embedding.jpg}};
%\draw[step=1cm, draw=gray] (pic.north west) grid (pic.south east);
%\draw[step=5cm, thick] (pic.north west) grid (pic.south east);
\node[label] at (0,-5) {CD8 memory};
\node[label] at (3,-7) {CD8 naive};
\node[label] at (-4,-6) {CD4 resting Thelper};
\node[label] at (-7,-5) {CD4 effector Thelper};
\node[label] (CD4rtr) at (-6,-8) {CD4 resting Treg};
\draw[arr] (CD4rtr) to (-6.7,-7.3);
\node[label] (CD4etr) at (-8,-8) {CD4 effector Treg};
\draw[arr] (CD4etr) to (-7.5,-7);
\node[label] (NK) at (-8,0) {NK};
\draw[arr] (NK) to (-6,-1.2);
\draw[arr] (NK) to (-7,-0.2);
\node[label] (NKT) at (-5,-3) {NKT};
\draw[arr] (NKT) to (-5.5,-2.5);
\draw[arr] (NKT) to (-6.2,-2.8);
\node[label] (gdtcr) at (-1,-4) {$\gamma\delta$TCR};
\draw[arr] (gdtcr) to (-1,-2.8);
\draw[arr] (gdtcr) to (-1.8,-3.8);
\node[label] (auto) at (-2,0) {autofluorescent cells};
\draw[arr] (auto) to (-2,-1);
\node[label] at (-6,4) {dead cells};
\node[label] at (3,1) {unmarked leukocytes};
\node[label] at (6,6) {debris};
\end{tikzpicture}
\caption{Larger version of Figure 5 --- an embedding of more than 1 billion cells from the IMPC dataset (\href{http://flowrepository.org/id/FR-FCM-ZYX9}{\tt FR-FCM-ZYX9}). The main lineage markers are highlighted as combinations of RGB colors: CD8 in red, CD4 in green and CD161 in blue. Annotations are made based on presence of several markers that are not highighted in the embedding, mainly the Live/Dead marker, $\gamma\delta$TCR, CD25, GITR, and fluorescence scatter intensities.}
\end{figure}

\clearpage
\begin{listing}[p]
\lstdefinelanguage{Julia}%
  {morekeywords={abstract,begin,break,case,catch,const,continue,do,else,elseif,%
      end,export,false,for,function,immutable,import,importall,if,in,%
      macro,module,otherwise,quote,return,switch,true,try,type,typealias,%
      using,while},%
   sensitive=true,%
   alsoother={$},%
   morecomment=[l]\#,%
   morecomment=[n]{\#=}{=\#},%
   morestring=[s]{"}{"},%
   morestring=[m]{'}{'},%
}[keywords,comments,strings]%
\begin{lstlisting}[basicstyle=\scriptsize\ttfamily,language=julia,keywordstyle=\color{blue},stringstyle=\color{magenta},commentstyle=\color{ForestGreen},showstringspaces=false, frame=single, rulecolor=\color{lightgray}]
using Distributed, GigaSOM, GigaScatter, ClusterManagers
import Glob, NearestNeighbors, Distributions

# find the file names to process
files = Glob.glob("TC_SPL_*.fcs")

# read the number of available workers from environment and start the worker processes
n_workers = parse(Int, ENV["SLURM_NTASKS"])
addprocs_slurm(n_workers, topology=:master_worker)

# load the required packages on all workers
@everywhere using GigaSOM
@everywhere using GigaScatter

# load the data to workers
dataset = loadFCSSet(:IMPCdata, files)

# collect the total number of cells from all workers
dataset_size = distributed_mapreduce(dataset, d->size(d,1), +)
@info "Loaded total $dataset_size cells."

# preprocess the distributed data
dselect(dataset, Vector(1:18)) # columns 1-18 contain interesting information
dtransform_asinh(dataset, Vector(7:18), 500.0) # transform the marker expressions
dscale(dataset, Vector(1:18)) # scale all columns

# train the SOM and run the embedding (this uses all available Slurm workers)
som = initGigaSOM(dataset, 32, 32)
som = trainGigaSOM(som, dataset, epochs=30,
                   rFinal=0.1, radiusFun=expRadius(-20.0),
                   knnTreeFun=NearestNeighbors.BallTree)
e = embedGigaSOM(som, dataset, k=16, output=:embeddedIMPC)

# setup constants for rasterization of the embedding
rasterSize = (2048, 2048)
alpha = 0.003
dist = Distributions.Normal()

# run the distributed rasterization
compound_raster = mixedRaster(distributed_mapreduce(
    [dataset, e],
    (d, e) -> begin
        # convert the normalized expressions to numbers between 0--1
        colors = hcat(
            Distributions.cdf.(dist, d[:,10]), # R: CD8 column
            Distributions.cdf.(dist, d[:,16]), # G: CD4 column
            Distributions.cdf.(dist, d[:,15]), # B: CD161 column
            fill(alpha, size(d,1)))
        # rasterize the local part of the data
        mixableRaster(rasterize(
            rasterSize,
	    Matrix{Float64}(e'),
            Matrix{Float64}(colors'),
            xlim=(-2,33), ylim=(-2,33)))
    end,
    mixRasters))

# save the result to a PNG
savePNG("impc-embedding.png", compound_raster)
\end{lstlisting}
\caption{Complete code for Julia workflow that produces the embedding of the IMPC dataset (Figures 5 and S4).}
\end{listing}
\end{document}
