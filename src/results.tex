\section{Results}
\label{sec:results}

The main result achieved by GigaSOM is the ability to quickly cluster and visualize datasets of previously unreachable size.
In particular, we show that building a SOM usable for FlowSOM-style analysis from $10^9$ cells with 40 parameters can be performed in minutes, even on relatively small compute clusters with just around a hundred of cores.
This performance achievement vastly simplifies the interactive work with large datasets, as the scientists may, for instance, try more combinations of hyperparameters and quickly get the feedback to improve the analysis and clustering of the huge dataset.

In this section, we first compare the output of GigaSOM.jl to that of FlowSOM, showing that the change in the SOM training algorithm has minimal impact on the quality of results.
Further, we provide benchmark results that confirm that GigaSOM.jl scales horizontally, and details of the speedup achievable by employing spatial indexing data structures for acceleration of the nearest-neighbor queries.
Finally, we demonstrate the achievable results by processing a giga-scale dataset from a recent study by the International Mouse Phenotyping Consortium (IMPC)~\cite{brown_international_2012}.

The presented performance benchmarks were executed on a Slurm-managed HPC cluster equipped with Intel\textregistered Xeon\textregistered E5-2650 CPUs; each node with 2 physical CPUs (total 24 cores) and 128GB of RAM.
All benchmarks were executed several times, the times were measured as `real' (wall-clock) time using the standard Julia timer facility.
Measurements of the first runs were discarded to prevent the influence of caching and Julia just-in-time compilation; remaining results were reduced to medians.

\subsection{Validation of clustering quality}

To compare the GigaSOM.jl output with the one from FlowSOM, we used a methodology similar to the one used by~\citet{weber_comparison_2016}.
The datasets were first processed by the clustering algorithms to generate clusters, which were then assigned to ground-truth populations so that the coverage of individual populations by clusters was reasonably high. The mean F1 score was then computed between the aggregated clusters and corresponding ground-truth populations.
While~ \citet{weber_comparison_2016} used a complicated method of cluster assignment optimization to find the assignment that produces the best possible mean F1 score, we employed a simpler (and arguably more realistic) greedy algorithm that assigns each generated cluster to a population with the greatest part covered by that cluster.

The benchmark did not consider metaclustering~\cite{gassen_flowsom:_2015}, since the comparison aimed to mainly detect the results caused by SOM training differences.

\begin{figure}
\centering
\includegraphics[width=3in]{plots/clustering-small.pdf}
\caption{Comparison of GigaSOM.jl results with manual gating of the \emph{Levine32} dataset.
The confusion matrix is normalized in rows, showing the ratio of cells in each aggregate of GigaSOM-originating clusters that matches the cell types from manual analysis.
Darker color represents better match.
The mean F1 score is comparable to FlowSOM. A more comprehensive comparison is available in Supplementary Figure S1.}
\label{fig:clust}
\end{figure}

For the comparison, we reused the datasets \texttt{Levine\_13dim} and \texttt{Levine32\_32dim} from the clustering benchmark~\cite{weber_comparison_2016}. In a typical outcome, most populations were matched by GigaSOM.jl just as well as by FlowSOM, as displayed in~\cref{fig:clust} (detailed view is available in supplementary figure S1). Both methods consistently achieved mean F1 scores in the range of 0.65--0.7 on the \texttt{Levine\_13dim} dataset and 0.81--0.84 on the \texttt{Levine\_32dim} dataset for a wide range of reasonable parameter settings. In the tests, neither algorithm showed a significantly better resulting mean F1 score.

\subsection{Scalable performance on large computer clusters}

\begin{figure*}
\centering
\includegraphics[width=6in]{plots/bench-soms.pdf}\\
\includegraphics[width=6in]{plots/bench-dims.pdf}
\caption{Performance dependency of distributed algorithms in GigaSOM on data dimensionality, SOM size and number of available workers.
Data processing performance is displayed as normalized to median speed in cells per second (c/s).}
\label{fig:benchmark}
\end{figure*}

\begin{figure}
\centering
\includegraphics[width=3in]{plots/trees-small.pdf}
\caption{Effect of data indexing structures on GigaSOM performance.
The plotted points show relative speedup of the algorithms utilizing kd-tress (horizontal axis) and ball-trees (vertical axis) compared to the brute-force neighbor search.
Baseline (1\texttimes\ speedup) is highlighted by thick grid lines --- a point plotted in the upper right quadrant represents a benchmark measurement that showed speedup for both kd-trees and ball-trees, upper left quadrant contains benchmark results where ball-trees provided speedup and kd-trees slowed the computation down, etc.}
\label{fig:indexing}
\end{figure}

The benchmark of implementation scalability was performed as follows:
A randomly generated dataset was distributed among the target computation nodes (CPUs) so that all nodes have an equal amount of data.
For the benchmark, node counts as powers of two up to 256 have been chosen while the numbers of dataset parameters were chosen from multiples of 10 up to 50.
The size of the dataset slice for a single node varied between 100, 200 and 300 thousand cells to verify the impact of data density in cluster.
The dataset was then processed by the SOM training algorithm for SOM sizes 10\texttimes10, 20\texttimes20 and 40\texttimes40.
The resulting SOMs were used for classifying the dataset into clusters (each input data point was assigned to a cluster defined by the nearest neighbor). An embedded view of the data was produced with the Julia implementation of EmbedSOM.
All algorithms were also tested in variants where the brute-force search for nearest neighbors (or k-neighborhoods in case of EmbedSOM) was replaced by constructing and using a spatial-indexing data structure, in particular by the kd-trees and ball-trees.

The scalability results are summarized in~\cref{fig:benchmark}: All three implemented algorithms scale almost linearly with the dataset size, the size of the SOM, and the dimension of the dataset. They reach an almost linear speedup with added compute capacity. The required inter-node communication in the case of SOM training caused only a negligible overhead in the training; the majority of which was caused by the random variance in execution time of computation steps on nodes. Consequently, the embarrassingly parallel classification and embedding algorithms behaved as expected. Detailed benchmark results that show precise energy requirements of the training per processed data-point, useful for deployment in large environments, are available in supplementary figure S2.

Influence of the spatial indexing on the speed of various operations was collected as relative speedups (or slow-downs) when compared to a naive search. The results are displayed in~\cref{fig:indexing}. We have observed that both kd-trees and ball-trees were able to accelerate some operations by a factor above 2\texttimes, but the use of spatial indexing suffered from many trade-offs that often caused performance decrease.

Most importantly, the cost of building the index has often surpassed the cost of neighborhood lookup by brute-force method, which is most easily observable on the measurements of ball-trees on lower SOM sizes.
Both trees have struggled to provide sufficient speedup in presence of higher dimensionality overhead (over 30), and had only negligible impact on the execution time of EmbedSOM computation, which was dominated by other operations.

On the other hand, it was easily possible to gain speedups around 1.5\texttimes for SOM training in most tests with lower dimension and large SOM, reaching 2.7\texttimes for a 20-dimensional dataset (typical for current flow cytometry) processed with large 40\texttimes40 SOM.
From the results, it seems appropriate to employ the spatial indexing when the cost of other operations outweighs the cost of building the index and the dimensionality overhead does not impede the efficiency of indexed lookup; in particular when training large SOMs of dimensionality less than around 30, and when per-node data occupancy is sufficiently high.
Detailed measurements for all SOM sizes and dataset dimensions are available in Supplementary Figure S3.

\subsection{HPC analysis of previously unreachable dataset sizes}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{media/embedding.jpg}
\caption{Raw IMPC Spleen T-cell dataset, processed by GigaSOM.jl and embedded by the Julia implementation of EmbedSOM.
The figure shows an aggregate of 1,167,129,317 individual cells.
Expression of three main markers is displayed in combination as mixed colors; CD8 in red, CD4 in green, and CD161 in blue.
A more detailed, annotated version of the visualization is available in Supplementary Figure S4.
}
\label{fig:embedding}
\end{figure}

To showcase the GigaSOM.jl functionality on a realistic dataset, we have used a large dataset from the IMPC phenotyping effort~\cite{brown_international_2012} that contains measurements of mouse spleens by a standardized T-cell targeting panel. with individual cohorts containing genetically modified animals (typically a single-gene knockouts) and controls; total 2905 samples contain 1,167,129,317 individual cells. (The dataset is available from FlowRepository under the accession ID \href{http://flowrepository.org/id/FR-FCM-ZYX9}{\tt FR-FCM-ZYX9}.)

The dataset was intentionally prepared by a very simple process --- cell expressions were compensated, fluorescent marker expressions were transformed by the common \emph{asinh} transformation with co-factor 500, and all dataset columns were scaled to $\mu=0$ and $\sigma=1$.
The resulting data were used to train a 32\texttimes32 SOM, which was in turn used to produce the embedding of the dataset (with EmbedSOM parameter $k=16$), which was rasterized. The final result can be observed in~\cref{fig:embedding}. The detailed workflow is shown in Supplementary Listing S1.

Notably, on a relatively small 256-core computer cluster (total 11 server nodes within a larger Slurm-managed cluster), the whole operation, consisting of Julia initialization, data loading (82.6GB of FCS files), SOM training for 30 epochs, embedding and export of embedded data (17.4GB) took slightly less than 25 minutes, and consumed at most 3GB of RAM per core.
From that, each epoch of the parallelized SOM training took around 25 seconds, and the computation of EmbedSOM visualization took 3 minutes.
Distributed plotting of the result was done using the \texttt{GigaScatter.jl} package; the parallel rasterization and combination of partial rasters took slightly over 4 minutes.
