\section{Methods}

The Kohonen Self-Organizing-Map (SOM) algorithm~\cite{kohonen_essentials_2013} is a kind of simplified neural network with a single layer equipped with a topology.
The task of the SOM training is to assign values to the neurons so that the training dataset is covered by neighborhoods of the neurons, and, at the same time, that the topology of the neurons is preserved in the trained network.
A 2-dimensional grid is one of the most commonly used topologies, because it simplifies interpretation of the results as neuron values positioned in the 2-dimensional space, and related visualization purposes (e.g.~EmbedSOM~\cite{kratochvil_generalized_2019}).
At the same time, the trained network can serve as a simple clustering of the input dataset, classifying each data point to its nearest neuron.

\subsection{Batch SOM training}

The original SOM training algorithm was introduced by~\citet{kohonen_self-organized_1982}.
The map is organized as a collection of randomly initialized vectors (called \emph{codebook}, with weights $W(1)$).
The training proceeds in iterations (indexed by time $t$), where in each iteration a randomly selected data point in the dataset is used to produce an updated codebook as
\[ {W_i(t+1) = W_i(t) + \alpha(t)h(t)\odot\left(x - W_i(t)\right)}, \]
where $\alpha$ is the learning rate parameter, $i$ is the neuron nearest to the randomly selected data point $x$, and $h$ is the vector of map-topology distances of the codebook vectors to the best matching unit.
The learning has been shown to converge after a predictable number of iterations if $\alpha$ and neighborhood size in $h$ and topological neighborhood size are gradually lowered~\cite{kohonen_essentials_2013}.

A more scalable variant of the algorithm can be obtained by running the single updates in batches where the values of $x$ are taken from the whole dataset at once; which can be expressed in matrix form
\[ W(t+1) = \hat{H}(t)\cdot \mathcal{N}(X,W(t))\cdot X, \]
where $\mathcal{N}(X,W(t))$ is a binary matrix that contains $1$ at position $i,j$ if and only if $W_i(t)$ is the closest codebook vector to $X_j$, and $\hat{H}(t)$ is a distance matrix of the codebook in 2D map topology with rows scaled to sum 1. Notably, the algorithm converges in the same cases as the online version~\cite{cheng_convergence_1997}, and may be viewed as a generalized version of k-means clustering, which is obtained by setting $H(t)=I$.)

Implementations of the batch training may employ several assumptions that are not available with the online training:
\begin{itemize}
\item computation of $\mathcal{N}$ can employ a pre-built spatial indexing structure on $W(t)$, which is constant for the whole batch,
\item all computations involving $X$ can be sliced and parallelized (moreover, because the accesses to $X$ are not randomized, the implementation is more cache-efficient and more suitable for SIMD- and GPU-based acceleration)
\item multiplication by $\hat{H}(t)$ can be associatively postponed to work only with the codebook-size matrix, saving more than 50\% required computation volume when compared to online training with large neighborhoods.
\end{itemize}

\subsection{Distributed implementation of GigaSOM.jl}

The officially registered GigaSOM.jl package is a flexible, horizontally scalable, HPC-aware version of the batch SOM training writen in the Julia programming language.
The language choice has allowed a reasonably high-level description of the problem suitable for easy customization, while still supporting the efficient low-level operations necessary for fast data processing.
GigaSOM.jl contains a library of functions for loading the data from Flow Cytometry Standard (FCS) files, distributing the data across a network to remote computation nodes present in the cluster, running the parallelized computation, and to exporting and visualizing the results.
The overall design of the main implemented operations is outlined in~\cref{fig:arch}.
Example Julia code that executes the distributed operations is provided in Supplementary Listing S1.

\begin{figure}[t]
{\sf\bfseries Data distribution}
\begin{center}
\input{src/fig-distribution.tex}
\end{center}
{\sf\bfseries Computation}
\begin{center}
\input{src/fig-processing.tex}
\end{center}
\caption{Architecture of GigaSOM.jl.
Top: Data distribution process divides the available FCS files into balanced slices, individual workers retrieve their respective slice data using a shared storage.
Below: The SOM learning and visualization processes require only a minimal amount of data transferred between the master and worker nodes; consisting of the relatively small codebook in case of SOM learning (blue arrows) and pre-rasterized graphics in case of visualization (green arrows).}
\label{fig:arch}
\end{figure}

\subsubsection{Data distribution procedure}

Distributed computation process in GigaSOM is structured such as each computation node (`worker') keeps its own, persistent slice of the whole dataset, and the sub-results from the nodes are aggregated by the master node.
To establish this structure, GigaSOM implements a separate procedure that aggregates the input FCS files and creates a balanced set of slices equally distributed among the workers.

The distribution procedure is implemented as illustrated in~\cref{fig:arch} (top):
First, the master node reads the headers and sizes of individual FCS files, verifying their structure and determining the total number of stored data points.
This is used to create minimal descriptions of equally-sized dataset slices (each description consists only of 4 numbers first and last file and first and last data point index), which are transferred to individual workers.
Each worker interprets its assigned slice description, and extracts the part of the data from the relevant FCS files saved on a shared storage.
The resulting slices may be easily exported to the storage and re-imported by individual workers, thus saving time if multiple analyses run on the same data (e.g., in case of several clustering and embedding runs with different parameters).

Importantly, a shared filesystem is usually one of the most efficient ways to perform high-bandwidth data transfers in HPC environments, which makes the dataset loading process relatively fast.
If a shared filesystem is not available, GigaSOM.jl also includes optional support for direct data distribution using the \texttt{Distributed.jl} package.

\subsubsection{Batch SOM implementation}

After the nodes are equipped with the data slices, the batch SOM training proceeds as illustrated in~\cref{fig:arch} (bottom):

\begin{enumerate}[label=\arabic*.]
\item The master node initializes the SOM codebook (usually by random sampling from available data).
\item The codebook is broadcast to all worker nodes. As the size of the usual codebook is at most several tens of kilobytes, data transfer speed does not represent a performance bottleneck in this case.
\item The workers calculate a partial codebook update on their data and send the results back to the master node.
\item Finally, the master node gathers the individual updates, multiplies the collected result by $\hat{H}(t)$, and continues with another iteration from step 2, if necessary.
\end{enumerate}

Technically, the GigaSOM.jl implementation of steps 2--4 follows the structure of MapReduce-style data processing~\cite{dean_mapreduce_2008}, which has allowed clear separation of parallel processing implementation from actual computation primitives, and improved long-term code maintainability.
Apart from simplifying the implementation of various algorithm modifications, the MapReduce-style abstractions enable future transition to more complex data handling routines, such as the support for distributed parallel broadcast and reduction that is required for handling huge SOMs on very large number of workers (\citet{collange_numerical_2015} provide a comprehensive discussion on that topic).

The time required to perform one iteration of the SOM training is mainly derived from the speed of the codebook transfer between nodes, and the amount of computation done by individual nodes.
The current GigaSOM.jl implementation transfers all codebooks directly between the master node and the workers, giving time complexity $\mathcal{O}(b)+\mathcal{O}(\frac{n}{c})$ for $b$ computation nodes equipped with $c$ CPUs, working on a dataset of size $n$.
This complexity can be improved to $\mathcal{O}(\log_2 b)+\mathcal{O}(\frac{n}{c})$ by using the aforementioned algorithms for parallel data broadcast and reduction, but we have not found a realistic dataset of size sufficient to gain any benefit from such optimization.

\subsubsection{Spatial indexing}

Since the most computationally expensive step of the SOM training is the search for nearest codebook vectors for each dataset item (i.e., construction of the matrix $\mathcal{N}$), we have evaluated the use of spatial indexing structures for accelerating this operation.
GigaSOM.jl implementation can employ the structures available in the \texttt{NearestNeighbors} package, which include kd-trees and ball trees (also called vantage-point trees).~\cite{bentley_multidimensional_1975,omohundro_five_1989}

Although the efficiency of spatial indexing is vastly reduced with increasing dataset dimensionality, the measurements in section \hyperref[sec:results]{Results} show that it can provide significant speedup with very large SOMs, even on data with more than 20 dimensions.

\subsubsection{Visualization support}

To simplify visualization of the results, GigaSOM.jl includes a parallel reimplementation of the fast EmbedSOM algorithm in Julia~\cite{kratochvil_generalized_2019}, which provides a quickly available, visually interpretable insight into the cell type distribution within the datasets.
EmbedSOM computes an embedding of the cells to 2-dimensional space, similarly as the popular t-SNE or UMAP algorithms~\cite{maaten_visualizing_2008,mcinnes_umap_2018}.
Unlike the usual dimensionality reduction algorithms, it uses the constructed SOM as a guiding manifold for positioning the individual points into the low-dimensional space, and achieves linear time complexity in the size of dataset.
The parallel implementation of EmbedSOM is built upon the same distributed data framework as the batch SOMs --- since EmbedSOM is embarrassingly parallel, it can be run directly on the individual data slices, and gain the same speedup from parallel processing.

In order to aid the plotting of the EmbedSOM output, we have additionally implemented a custom scatterplot rasterizer in package \texttt{GigaScatter.jl}, which includes functions for quick plotting of large amounts of low-alpha points.
The rasterization can also be performed in a distributed manner within the MapReduce-style operations, as shown in Supplementary Listing S1.
