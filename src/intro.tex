\section{Background}

Advances in single-cell technologies, such as Mass Cytometry (CyTOF), Single-Cell RNA Sequencing (scRNA) and Spectral Flow Cytometry~\cite{bandura_mass_2009,jaitin_massively_2014,schmutz_spectral_2016}, provide deep and comprehensive insights into the complex mechanism of cellular systems, such as immune cells in blood, tumor cells and their microenvironments, and various microbiomes, including the single-celled marine life ecosystems.
Mass cytometry and spectral cytometry have enabled staining of the cells with more than 40 different markers to discover cellular differences under multiple conditions.
The samples collected in recent studies often contain millions of measured cells (events), resulting in large and high-dimensional datasets.
Traditional analysis methods, based on manual observation and selection of the clusters in 2D scatter-plots, is becoming increasingly difficult to apply on data of such complexity:
For high-dimensional data, this procedure is extremely laborious, and the results often carry researcher or analysis bias~\cite{mair_end_2016}.

Various dimensionality reduction, clustering, classification and data mining methods have been employed to aid with the semi- or fully-automated processing, including the neural networks~\cite{arvaniti_sensitive_2017}, various rule-based and tree-based classifiers in combination with clustering and visualization~\cite{bruggner_automated_2014,qiu_extracting_2011}, or locality-sensitive and density-based statistical approaches~\cite{lun_testing_2017}.
However, computational performance of the algorithms, necessary for scaling to larger datasets, is often neglected, and the available analysis software often relies on various simplifications (usually downscaling) required to process large datasets in reasonable time, without disproportionate hardware requirements.

To improve the performance, the underlying algorithm of FlowSOM~\cite{gassen_flowsom:_2015} introduced a combination of the Self-Organizing-Maps (SOMs) by~\citet{kohonen_essentials_2013} and metaclustering, which allowed efficient and accurate clustering of millions of cells~\cite{weber_comparison_2016}.
FlowSOM is currently available as an R package that became an essential part of many workflows, analysis pipelines and software suites, including FlowJo and Cytobank\textregistered.
Despite of the advance, the amount of data generated in large research-oriented and clinical studies frequently grows to hundreds of millions of cells, processing of which requires not only the efficiency of the algorithm, but also a practical scalable implementation.

Here, we present GigaSOM.jl, an implementation of the SOM-based clustering and dimensionality-reduction functionality using the Julia programming language~\cite{bezanson_julia:_2012}.
Compared to FlowSOM, GigaSOM.jl provides two major improvements:
First, it utilizes the computational and memory resources efficiently, enabling it to process datasets of size larger than $10^8$ cells on commonly available hardware.
Second, the implementation provides horizontal scaling support, and can thus utilize large high-performance computing clusters (HPC) to gain improvements in speed and tangible dataset size.
Additionally, the implementation in Julia is sufficiently high-level for allowing easy extensibility and cooperation with other tools in Julia ecosystem.
Several technical limitations imposed by the R-wrapped implementation in the C programming language of FlowSOM are also overcome.
